    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#error_email").hide();
        $("#price").hide();
        $("#error_email2").hide();
        $("#error_email3").hide();
        $("#error_name").hide();
        $("#error_name2").hide();
        $("#error_role").hide();
        $("#error_country").hide();
        $("#error_state").hide();
        $("#error_city").hide();
        
        $("#error_seller").hide();
        $("#edit-error_email").hide();
        $("#edit-error_email2").hide();
        $("#edit-error_email3").hide();
        $("#edit-error_name").hide();
        $("#edit-error_name2").hide();
        $("#edit-error_role").hide();

        $("#error_price").hide();
        $("#error_customer").hide();
        $("#error_seller").hide();
        $("#error_customer").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-user-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function edit_user_popup(email,id,name,role){
        $( "#edit-email" ).val(email);
        $( "#edit-user-id" ).val(id);
        $( "#edit-name" ).val(name);
        if(role=='admin')
            roleOption = "<option value='admin' selected>Admin</option><option value='user'>User</option>";
        else
            roleOption = "<option value='admin'>Admin</option><option value='user' selected>User</option>";

        $( "#edit-role" ).html(roleOption);
        $('#editUserSubmit').attr("onclick","update_user_details("+id+")");
    }

    function deactivate_confirmation(email,id){
        $( "#user-email" ).html(email);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+email+"',"+id+")");
    }

    function reset_confirmation(email,id){
        $( "#reset-user-email" ).html(email);
        $('#resetYesButton').attr("onclick","reset_submit('"+email+"',"+id+")");
    }

    function deactivate_submit(email,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/deactivate_user/"+email+"/"+id,
            cache: false,
            success: function (result) {
                var result = $.parseJSON(result);
                if(result.status=='success'){
                    location.reload();
                }
                else{
                    alert("Oops Algo salió mal!");
                }
            },
            error: ajax_error_handling
        });
    }

    function reset_submit(email,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/reset_user_password/"+email+"/"+id,
            cache: false,
            success: function (result) {
                var result = $.parseJSON(result);
                if(result.status=='success'){
                    location.reload();
                }
                else{
                    alert("Oops Algo salió mal!");
                }
            },
            error: ajax_error_handling
        });
    }

    $('#country').on('change',function(){
        country_id = this.value;
        
        $.ajax({
            url: $("#base-url").val()+"admin/get_states/"+country_id,
            cache: false,
            dataType: 'json',
            success: function (result) {
                a=1;
                $('#state')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected="selected">-- Departamento --</option>')
                        .val('0'); 
                $('#city')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected="selected">-- Ciudad --</option>')
                        .val('0');
                $.each(result, function(key, state) { 
                     
                    $('#state').append($("<option></option>").attr("value",state.id).text(state.name)); 
                });
            },
            error: ajax_error_handling
        });
    });

    $('#state').on('change',function(){
        state_id = this.value;
        
        $.ajax({
            url: $("#base-url").val()+"admin/get_cities/"+state_id,
            cache: false,
            dataType: 'json',
            success: function (result) {
                $('#city')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option value="0" selected="selected">-- Ciudad --</option>')
                        .val('0'); 
                $.each(result, function(key, city) { 
                     
                    $('#city').append($("<option></option>").attr("value",city.id).text(city.name)); 
                });
            },
            error: ajax_error_handling
        });
    });

    $('#customer').on('change',function(){
        
        $("#price").val('');
        $("#price_visitor").val('');

        customer_id = this.value;
        
        $.ajax({
            url: $("#base-url").val()+"admin/get_user/"+customer_id,
            cache: false,
            
            success: function (result) {
                var result = $.parseJSON(result);
                $.each(result, function(key, customer) { 
                     console.log(customer);
                     
                    $('#quota').val(customer.quota);
                    $('#current_quota').val(customer.current_quota);
                    $('#percent_visitor').val(customer.percent_visitor);
                    $("#price").show();
                });
            },
            error: ajax_error_handling
        });
    });

    $('#price').on('input',function(){
        var price = this.value;
        var percent_visitor= $('#percent_visitor').val();
        var price_visitor = price * percent_visitor;
        $('#price_visitor').val(price_visitor); 
    });

    function update_user_details(id){
        hideErrorMessages();
        show_loading();
        var i=0;
        var name = $('#edit-name').val().trim();
        var email = $('#edit-email').val().trim();
        var role = $('#edit-role').val();


        if(name == ""){
            $("#edit-error_name").show();
            i++;
        }
        else if (!name.match(/^[A-Za-z0-9\s]+$/)) {
            $("#edit-error_name2").show();
            i++;
        }

        if(email == ""){
            $("#edit-error_email").show();
            i++;
        }
        else if (!email.match(/^[\w -._]+@[\-0-9a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/)) {
            $("#edit-error_email3").show();
            i++;
        }

        if(role == 0){
            $("#edit-error_role").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"admin/update_user_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {email: email, id:id, name:name, role:role},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_email2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops Algo salió mal!");
                    }
                },
                error: ajax_error_handling
            });
        }
    }


   $('#recordVisit').click(function(){
        hideErrorMessages();
        show_loading();
        var i=0;
        var seller = $('#seller').val();
        var customer = $('#customer').val();
        var price = $('#price').val().trim();
        var quota = $('#quota').val();
        var current_quota = $('#current_quota').val();
        var price_visitor = $('#price_visitor').val();
        var comments = $('#comments').val();

        if(seller == ""){
            $("#error_seller").show();
            i++;
        }
        

        if(customer == ""){
            $("#error_customer").show();
            i++;
        }
        

        if(price == ""){
            $("#error_price").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val() + "admin/add_visit",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {seller:seller, user_id:customer, price:price, price_visitor:price_visitor, comments:comments, quota:quota, current_quota:current_quota},
                success: function (result) {
                    var result = $.parseJSON(result);
                    location.reload();
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
   });



    $( "#newUserSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var name = $('#name').val().trim();
        var email = $('#email').val().trim();
        var role = $('#role').val();
        var city = $('#city').val();
        var country = $('#country').val();
        var nit = $('#nit').val();
        var address = $('#address').val();
        var phone = $('#phone').val();
        var quota = $('#quota').val();

        if(name == ""){
            $("#error_name").show();
            i++;
        }
        else if (!name.match(/^[A-Za-z0-9\s]+$/)) {
            $("#error_name2").show();
            i++;
        }

        if(email == ""){
            $("#error_email").show();
            i++;
        }
        else if (!email.match(/^[\w -._]+@[\-0-9a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/)) {
            $("#error_email3").show();
            i++;
        }

        if(role == 0){
            $("#error_role").show();
            i++;
        }

        if(city == ""){
            $("#error_city").show();
            i++;
        }

        if(state == ""){
            $("#error_state").show();
            i++;
        }

        if(country == ""){
            $("#error_country").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val() + "admin/add_user",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {email:email, role:role, name:name, quota:quota, phone:phone, address:address, nit:nit, city:city},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#error_email2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops Algo salió mal!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });

    


