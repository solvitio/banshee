<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }

        if($this->session->userdata('role') != 'admin'){
            redirect(base_url());
        }

        $this->load->model('admin_model');
    }
    

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function user_list(){

        $data = array(
            'formTitle' => 'Administración de Usuarios',
            'title' => 'Administración de Usuario',
            'users' => $this->admin_model->get_user_list(),
            'countries' => $this->admin_model->get_countries_list(),
        );

        
        $this->load->view('admin/user_list', $data);

    }

    public function visitors(){

        $data = array(
            'formTitle' => 'Administración de Usuarios',
            'title' => 'Administración de Usuario',
            'customers' => $this->admin_model->get_user_by_type("customer"),
            'sellers' => $this->admin_model->get_user_by_type("seller"),
            'visitors' => $this->admin_model->get_visitors(),
        );

        
        $this->load->view('admin/visitors', $data);

    }

    function add_user(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $insert = $this->admin_model->insert_user($postData);
        if($insert['status'] == 'success')
            $this->session->set_flashdata('success', 'Usuario '.$postData['email'].' Ha sido creado éxitosamente!');

        echo json_encode($insert);
    }

    function add_visit(){
        $this->ajax_checking();
        $postData = $this->input->post();
        $insert = $this->admin_model->insert_visitor($postData);
        if($insert['status'] == 'success')
            $this->session->set_flashdata('success', 'Visita Ha sido creada éxitosamente!');

        echo json_encode($insert);
    }

    function update_user_details(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->admin_model->update_user_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$postData['email'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function deactivate_user($email,$id){
        $this->ajax_checking();

        $update = $this->admin_model->deactivate_user($email,$id);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$email.' has been successfully deleted!');

        echo json_encode($update);
    }

    function reset_user_password($email,$id){
        $this->ajax_checking();

        $update = $this->admin_model->reset_user_password($email,$id);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$email.'`s password has been successfully reset!');

        echo json_encode($update);
    }

    function activity_log(){
        $data = array(
            'formTitle' => 'Monitor de Actividades',
            'title' => 'Monitor de Actividades',
        );
        $this->load->view('admin/activity_log', $data);

    }

    function get_activity_log(){
        $this->ajax_checking();
        echo  json_encode( $this->admin_model->get_activity_log() );
    }

    function get_states($country_id){
        $this->ajax_checking();

        echo  json_encode( $this->admin_model->get_states($country_id) );
    }

    function get_cities($state_id){
        $this->ajax_checking();

        echo  json_encode( $this->admin_model->get_cities($state_id) );
    }

    function get_user($user_id){
        $this->ajax_checking();

        echo  json_encode( $this->admin_model->get_user_by_id($user_id) );
    }



}

/* End of file */
