<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('admin_model');
    }

    public function index() {
        $data['users'] = $this->admin_model->get_user_list();
        $data['customers'] = $this->admin_model->get_user_by_type('customer');
        $data['sellers'] = $this->admin_model->get_user_by_type('seller');
        $data['visitors'] = $this->admin_model->get_visitors();
        $this->load->view('dashboard',$data);
        
    }


}

/* End of file */
