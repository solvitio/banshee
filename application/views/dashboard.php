<?php $this->load->view('partials/header'); ?>
  <?php $this->load->view('partials/sidebar_nav'); ?>
        <div id="page-wrapper">
            <?php if($this->session->flashdata('success')):?>
                &nbsp;<div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>
                &nbsp;<div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Dashboard</h3>
                </div>
                
                <!-- /.col-lg-12 -->
            </div>

            <div class="row">
                
                <div class="col-lg-3">
                    <div class="row text-center">
                        <div class="panel panel-success"> <div class="panel-heading"> <h3 class="panel-title">Usuarios</h3> </div> <div class="panel-body"> <?php echo count($users); ?> </div> </div>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="row text-center">
                        <div class="panel panel-danger"> <div class="panel-heading"> <h3 class="panel-title">Clientes</h3> </div> <div class="panel-body"> <?php echo count($customers); ?> </div> </div>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="row text-center">
                        <div class="panel panel-info"> <div class="panel-heading"> <h3 class="panel-title">Vendedores</h3> </div> <div class="panel-body"> <?php echo count($sellers); ?> </div> </div>
                    </div>
                    
                </div>
                <div class="col-lg-3">
                    <div class="row text-center">
                        <div class="panel panel-primary"> <div class="panel-heading"> <h3 class="panel-title">Visitas</h3> </div> <div class="panel-body"> <?php echo count($visitors); ?> </div> </div>
                    </div>
                    
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- wrapper -->
            
<?php $this->load->view('partials/footer'); ?>




