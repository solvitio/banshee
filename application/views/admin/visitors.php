<?php $this->load->view('partials/header'); ?>
  <?php $this->load->view('partials/sidebar_nav'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default"> 
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div role="tabpanel">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="advance">
                                        <div class="dataTable_wrapper" style="overflow: auto;">    
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-user-list">
                                                <thead>
                                                    <tr>
                                                        <th>Fecha</th>
                                                        <th>Vendedor</th>
                                                        <th>Visitante</th>
                                                        <th>Precio</th>
                                                        <th>Precio Visitante</th>
                                                        <th>Observaciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($visitors  as $visitor): ?>
                                                    <tr>
                                                        <td><?php echo $visitor->date; ?></td> 
                                                        <td><?php echo $visitor->seller_name; ?></td>
                                                        <td><?php echo $visitor->customer_name ?></td>
                                                        <td><?php echo $visitor->price; ?></td>
                                                        <td><?php echo $visitor->visitor_price; ?></td>
                                                        <td><?php echo $visitor->comments ?></td>
                                                        
                                                        
                            

                                                    </tr>
                                                    <?php endforeach; ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user" src="<?=base_url()?>assets/images/add.png" data-toggle="modal" data-target="#addUser" />
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

       




        <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Registrar una visita</h4>
                    </div>
                    <div class="modal-body">
                        
                      <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Vendedor</label>&nbsp;&nbsp;
                                    <label class="error" id="error_seller"> Campo Requerido.</label>
                                    <select name="seller" id="seller" class="form-control" >
                                        <option value="" selected="selected"> -- Vendedor --</option>
                                        <?php foreach($sellers as $seller) : ?>
                                            <option value="<?php echo $seller->user_id;?>" > <?php echo $seller->name;?> </option>
                                        <?php endforeach; ?>
                                        
                                    </select> 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Cliente</label>&nbsp;&nbsp;
                                    <label class="error" id="error_customer"> Campo Requerido.</label>
                                    <select name="customer" id="customer" class="form-control" >
                                        <option value="" selected="selected"> -- Clientes -- </option>
                                        <?php foreach($customers as $customer) : ?>
                                            <option value="<?php echo $customer->user_id;?>" > <?php echo $customer->name;?> </option>
                                        <?php endforeach; ?>
                                    </select> 
                                </div>
                            </div>
                      </div>

                       <div class="row">
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="error" id="error_price">Valor Neto</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="price" placeholder="Valor Neto" name="price" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="error" id="error_price_visitor">Precio Visita</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="price_visitor" disabled placeholder="Precio Visita" name="price_visitor" type="text" autofocus>
                                </div> 
                            </div>
                        </div>

                        <div class="row">
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label id="error_percent_visitor">Porcentaje de Visitas</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="percent_visitor" disabled placeholder="Porcentaje de visitas" name="percent_visitor" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label  id="error_quota">Cupo</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="quota" placeholder="Cupo" name="quota" type="text" disabled autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label id="error_current_quota">Saldo de Cupo</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="current_quota" disabled placeholder="Saldo de Cupo" name="current_quota" type="text" autofocus>
                                </div> 
                            </div>
                        </div>
                      
                      <div class="row">
                            
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="">Observaciones</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="comments" placeholder="Cometarios" name="comments" type="text" autofocus>
                                </div> 
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                        <button id="recordVisit" type="button" class="btn btn-primary">REGISTRAR</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->



       
        <!-- /#page-wrapper -->
        <?php $this->load->view('partials/footer'); ?>
        <script src="<?=base_url()?>assets/js/view/user_list.js"></script>