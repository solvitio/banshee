<?php $this->load->view('partials/header'); ?>
  <?php $this->load->view('partials/sidebar_nav'); ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default"> 
                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div role="tabpanel">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="advance">
                                        <div class="dataTable_wrapper" style="overflow: auto;">    
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-user-list">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Email</th>
                                                        <th>Rol</th>
                                                        <th>País</th>
                                                        <th>Departamento</th>
                                                        <th>Ciudad</th>
                                                        <th>Teléfono</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($users  as $user): ?>
                                                    <tr>
                                                        <td><?php echo $user->user_name; ?></td> 
                                                        <td><?php echo $user->email; ?></td>
                                                        <td><?php echo ucfirst($user->role) ?></td>
                                                        <td><?php echo $user->country_name; ?></td>
                                                        <td><?php echo $user->state_name; ?></td>
                                                        <td><?php echo $user->city_name; ?></td>
                                                        <td><?php echo $user->phone; ?></td>
                                                        
                                                        
                                                        <td>
                                                            <a class="btn btn-primary btn-sm" id="user-edit"  onclick="edit_user_popup('<?=$user->email?>','<?=$user->user_id?>','<?=$user->user_name?>','<?=$user->role?>');" data-toggle="modal" data-target="#editUser"> EDITAR </a>
                                                            <a class="btn btn-warning btn-sm" id="user-riset" onclick="reset_confirmation('<?=$user->email?>','<?=$user->user_id?>')" data-toggle="modal" data-target="#resetConfirm"> RESETEAR </a>
                                                            <a class="btn btn-danger btn-sm" id="user-delete" onclick="deactivate_confirmation('<?=$user->email?>','<?=$user->user_id?>');" data-toggle="modal" data-target="#deactivateConfirm"> ELIMINAR </a>
                                                            
                                                        </td>

                                                    </tr>
                                                    <?php endforeach; ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user" src="<?=base_url()?>assets/images/add.png" data-toggle="modal" data-target="#addUser" />
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>



        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">CONFIRMACIÓN PARA ELIMINAR USUARIO</h4>
                    </div>
                    <div class="modal-body">
                        <label>Está seguro de eliminar usuario? <label id="user-email" style="color:blue;"></label>.</label><br/>
                        <label>Click en <b>SI</b> para continuar.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Sí</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal -->
        <div class="modal fade" id="resetConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">CONFIRMAR RESETEO DE CONTRASEÑA</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to reset user <label id="reset-user-email" style="color:blue;"></label>'s password.</label><br/>
                        <label>Tempolary password will be sent to this email.</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="resetYesButton" class="btn btn-warning" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->




        <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">CREAR USUARIO NUEVO</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nombre</label> &nbsp;&nbsp;
                                    <label class="error" id="error_name"> El Campo es requerido.</label>
                                    <label class="error" id="error_name2"> El nombre debe ser alfanúmerico.</label>
                                    <input class="form-control" id="name" placeholder="Name" name="name" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="error_email"> Campo Requerido.</label>
                                    <label class="error" id="error_email2"> Email ya registrado.</label>
                                    <label class="error" id="error_email3"> Email inválido.</label>
                                    <input class="form-control" id="email" placeholder="E-mail" name="email" type="email" autofocus>
                                </div> 
                            </div>
                        </div>
                      <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>País</label>&nbsp;&nbsp;
                                    <label class="error" id="error_country"> Campo Requerido.</label>
                                    <select name="country" id="country" class="form-control" >
                                        <option value="" selected="selected"> -- País --</option>
                                        <?php foreach ($countries as $country) : ?>
                                            <option value="<?php echo $country->id ?>" ><?php echo $country->name ?></option>
                                        <?php endforeach; ?>
                                        
                                    </select> 
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Departamento</label>&nbsp;&nbsp;
                                    <label class="error" id="error_state"> Campo Requerido.</label>
                                    <select name="state" id="state" class="form-control" >
                                        <option value="" selected="selected"> -- Departamento -- </option>
                                    </select> 
                                </div>
                            </div>
                      </div>
                      <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Ciudad</label>&nbsp;&nbsp;
                                    <label class="error" id="error_city"> Campo Requerido.</label>
                                    <select name="city" id="city" class="form-control" >
                                        <option value="" selected="selected"> -- Ciudad --</option>
                                     </select> 
                                </div>
                            </div>
                            
                      </div>
                      <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nit</label> &nbsp;&nbsp;
                                   
                                    <input class="form-control" id="nit" placeholder="Nit" name="nit" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Dirección</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="address" placeholder="Dirección" name="address" type="text" autofocus>
                                </div> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Teléfono</label> &nbsp;&nbsp;
                                   
                                    <input class="form-control" id="phone" placeholder="Teléfono" name="phone" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Cupo</label> &nbsp;&nbsp;
                                    
                                    <input class="form-control" id="quota" placeholder="Cupo" name="quota" type="text" autofocus>
                                </div> 
                            </div>
                        </div>

                      <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Rol</label>&nbsp;&nbsp;
                                    <label class="error" id="error_role"> Campo Requerido.</label>
                                    <select name="role" id="role" class="form-control" >
                                        <option value="0" selected="selected">-- Seleccionar Rol --</option>
                                        <option value="admin">Administrador</option>
                                        <option value="user">Usuario</option>
                                        <option value="customer">Cliente</option>
                                        <option value="seller">Vendedor</option>
                                    </select> 
                                </div>
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                        <button id="newUserSubmit" type="button" class="btn btn-primary">CREAR</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">ACTUALIZAR DETALLES</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-user-id" value=""/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nombre</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_name"> Campo Requerido.</label>
                                    <label class="error" id="edit-error_name2"> El nombre debe ser alfanumérico.</label>
                                    <input class="form-control" id="edit-name" placeholder="Name" name="edit-name" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_email"> El campo es requerido.</label>
                                    <label class="error" id="edit-error_email2"> Email ya existe.</label>
                                    <label class="error" id="edit-error_email3"> Email iválido.</label>
                                    <input class="form-control" id="edit-email" placeholder="E-mail" name="edit-email" type="email" autofocus>
                                </div> 
                            </div>
                      </div>
                      <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <label class="error" id="edit-error_role"> Campo Requerido.</label>
                                    <select name="role" id="edit-role" class="form-control" >
                                    </select> 
                                </div>
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                        <button id="editUserSubmit" type="button" class="btn btn-primary">ACTUALIZAR</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
       
        <!-- /#page-wrapper -->
        <?php $this->load->view('partials/footer'); ?>
        <script src="<?=base_url()?>assets/js/view/user_list.js"></script>