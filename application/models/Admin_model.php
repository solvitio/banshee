<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    function get_user_list(){
        $this->db->select('*,cities.name as city_name,states.name as state_name,countries.name as country_name,users.name as user_name');
        $this->db->from('users');
        $this->db->where('status', 1);
        $this->db->join('cities','cities.id=users.city_id');
        $this->db->join('states','states.id=cities.state_id');
        $this->db->join('countries','countries.id=states.country_id');
        $query=$this->db->get();
        return $query->result();
    }

    function get_user_by_id($userID){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_id', $userID);
        $query=$this->db->get();
        return $query->result_array();
    }

    function validate_email($postData){
        $this->db->where('email', $postData['email']);
        $this->db->where('status', 1);
        $this->db->from('users');
        $query=$this->db->get();

        if ($query->num_rows() == 0)
            return true;
        else
            return false;
    }

    function insert_user($postData){

        $validate = $this->validate_email($postData);

        if($validate){
            $password = $this->generate_password();
            $data = array(
                'email' => $postData['email'],
                'name' => $postData['name'],
                'address' => $postData['address'],
                'phone' => $postData['phone'],
                'quota' => $postData['quota'],
                'current_quota' => $postData['quota'],
                'nit' => md5($postData['nit']),
                'role' => $postData['role'],
                'password' => md5($password),
                'created_at' => date('Y\-m\-d\ H:i:s A'),
                'city_id' => $postData['city'],
            );
            $this->db->insert('users', $data);

            /* $message = "Estos son los detalles de su cuenta:<br><br>Email: ".$postData['email']."<br>Contraseña Temporal: ".$password."<br>Por favor cambie su contraseña después de loguear.<br><br> tu puedes loguear desde ".base_url().".";
            $subject = "Creación de nueva cuenta";
            $this->send_email($message,$subject,$postData['email']);

            $module = "Administración de Usuario";
            $activity = "agregar usuario ".$postData['email'];
            $this->insert_log($activity, $module); */
            return array('status' => 'success', 'message' => '');

        }else{
            return array('status' => 'exist', 'message' => '');
        }

    }

    function update_user_details($postData){

        $oldData = $this->get_user_by_id($postData['id']);

        if($oldData[0]['email'] == $postData['email'])
            $validate = true;
        else
            $validate = $this->validate_email($postData);

        if($validate){
            $data = array(
                'email' => $postData['email'],
                'name' => $postData['name'],
                'role' => $postData['role'],
            );
            $this->db->where('user_id', $postData['id']);
            $this->db->update('users', $data);

            $record = "(".$oldData[0]['email']." to ".$postData['email'].", ".$oldData[0]['name']." to ".$postData['name'].",".$oldData[0]['role']." to ".$postData['role'].")";

            $module = "User Management";
            $activity = "update user ".$oldData[0]['email']."`s details ".$record;
            $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => $record);
        }else{
            return array('status' => 'exist', 'message' => '');
        }

    }


    function deactivate_user($email,$id){

        $data = array(
            'status' => 0,
        );
        $this->db->where('user_id', $id);
        $this->db->update('users', $data);

        $module = "User Management";
        $activity = "delete user ".$email;
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function reset_user_password($email,$id){

        $password = $this->generate_password();
        $data = array(
            'password' => md5($password),
        );
        $this->db->where('user_id', $id);
        $this->db->update('users', $data);

        $message = "Your account password has been reset.<br><br>Email: ".$email."<br>Tempolary password: ".$password."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        $subject = "Password Reset";
        $this->send_email($message,$subject,$email);

        $module = "User Management";
        $activity = "reset user ".$email."`s password";
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function generate_password(){
        $chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789!@#$%^&*()_=";
        $password = substr( str_shuffle( $chars ), 0, 10 );

        return $password;
    }

    function insert_log($activity, $module){
        $id = $this->session->userdata('user_id');

        $data = array(
            'user_id' => $id,
            'activity' => $activity,
            'module' => $module,
            'created_at' => date('Y\-m\-d\ H:i:s A')
        );
        $this->db->insert('activity_log', $data);
    }

    function get_activity_log(){
       /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        
        $aColumns = array('date_time', 'activity', 'email', 'module');
        $aColumnsWhere = array('activity_log.created_at', 'activity', 'email', 'module');
        $aColumnsJoin = array('activity_log.created_at as date_time', 'activity', 'email', 'module');

        // DB table to use
        $sTable = 'activity_log';
    
        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);
    
        // Paging
        if(isset($iDisplayStart) && $iDisplayLength != '-1')
        {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }
        
        // Ordering
        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
    
                if($bSortable == 'true')
                {
                    
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                    
                }
            }
        }
        
        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if(isset($sSearch) && !empty($sSearch))
        {
            for($i=0; $i<count($aColumns); $i++)
            {
                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                
                // Individual column filtering
                if(isset($bSearchable) && $bSearchable == 'true')
                {
                    $this->db->or_like($aColumnsWhere[$i], $this->db->escape_like_str($sSearch));

                }
            }
        }
        
        // Select Data
        $this->db->join('users', 'activity_log.user_id = users.user_id', 'left');
        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumnsJoin)), false);
        $rResult = $this->db->get($sTable);
    
        // Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
    
        // Total data set length
        $iTotal = $this->db->count_all($sTable);
    
        // Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );
        
        foreach($rResult->result_array() as $aRow)
        {
            $row = array();
            
            foreach($aColumns as $col)
            {
                if($col == 'date_time') $aRow[$col] = preg_replace('/\s/','<br />',$aRow[$col]);
                $row[] = $aRow[$col];
            }
    
            $output['aaData'][] = $row;
        }
    
        return $output;
    }


    function send_email($message,$subject,$sendTo){
        require_once APPPATH.'libraries/mailer/class.phpmailer.php';
        require_once APPPATH.'libraries/mailer/class.smtp.php';
        require_once APPPATH.'libraries/mailer/mailer_config.php';
        include APPPATH.'libraries/mailer/template/template.php';
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try
        {
            $mail->SMTPDebug = 1;  
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl'; 
            $mail->Host = HOST;
            $mail->Port = PORT;  
            $mail->Username = GUSER;  
            $mail->Password = GPWD;     
            $mail->SetFrom(GUSER, 'Administrator');
            $mail->Subject = "NO RESPONDER - ".$subject;
            $mail->IsHTML(true);   
            $mail->WordWrap = 0;


            $hello = '<h1 style="color:#333;font-family:Helvetica,Arial,sans-serif;font-weight:300;padding:0;margin:10px 0 25px;text-align:center;line-height:1;word-break:normal;font-size:38px;letter-spacing:-1px">Hello, &#9786;</h1>';
            $thanks = "<br><br><i>Esto es generado automáticamente, por favor no responder.</i><br/><br/>Thanks,<br/>Admin<br/><br/>";

            $body = $hello.$message.$thanks;
            $mail->Body = $header.$body.$footer;
            $mail->AddAddress($sendTo);

            if(!$mail->Send()) {
                $error = 'Error de mensaje: '.$mail->ErrorInfo;
                return array('status' => false, 'message' => $error);
            } else { 
                return array('status' => true, 'message' => '');
            }
        }
        catch (phpmailerException $e)
        {
            $error = 'Error al enviar mensaje: '.$e->errorMessage();
            return array('status' => false, 'message' => $error);
        }
        catch (Exception $e)
        {
            $error = 'Error: '.$e->getMessage();
            return array('status' => false, 'message' => $error);
        }
        
    }

    function get_countries_list(){
        $this->db->select('*');
        $this->db->from('countries');
        $query=$this->db->get();
        return $query->result();
    }

    function get_states($country_id){
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('country_id',$country_id);
        return $this->db->get()->result();
       
    }

    function get_cities($state_id){
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('state_id',$state_id);
        return $this->db->get()->result();
    }

    function get_visitors(){
        $this->db->select('*,sellers.name as seller_name,customers.name as customer_name');
        $this->db->from('visitors');
        $this->db->join('users AS sellers','sellers.user_id=visitors.seller');
        $this->db->join('users AS customers','customers.user_id=visitors.user_id');
        return $this->db->get()->result();
    }

    function get_user_by_type($type){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('status', 1);
        $this->db->where('role', $type);

        $query=$this->db->get();
        return $query->result();
    }

    function insert_visitor($postData){

        $current_quota = (int) $postData['current_quota'] - 1;
        $percent_visitor = round($current_quota * 100 / $postData['quota'],2);
        $data_user=array(
            'current_quota' => $current_quota,
            'percent_visitor' => $percent_visitor,
            );
        if($this->update_user_visits($postData['user_id'],$data_user)){
            $data=array(
                'seller' => $postData['seller'],
                'price' => $postData['price'],
                'visitor_price' => $postData['price_visitor'],
                'comments' => $postData['comments'],
                'user_id' => $postData['user_id'],
            );
            $this->db->insert('visitors', $data); 
            return $this->db->trans_complete();
        }else{

        }
    }

    function update_user_visits($user_id,$data){
        $this->db->trans_start();
        $this->db->where('user_id', $user_id);
        return $this->db->update('users', $data); 

    }

    
}

/* End of file */
